# Waveshare e-Paper on NanoPi without root

This is a patch for the original e-Paper driver package available at
https://github.com/waveshare/e-Paper. The patch brings the following features:

1. NanoPi SBC support, and
2. Ability to control the EPD as a normal user - when appropriate permissions
   are set on the `/dev/gpiochip0` and /dev/spidev0.0 devices.

The second feature means you can control your EPD without `sudo`, and you do
not need custom udev rules to do that.

# Usage

```bash
git clone https://github.com/waveshare/e-Paper.git waveshare_epd
git clone https://github.com/kocielnik/e-Paper.git epd_patch
cp epd_patch/* waveshare_epd/RaspberryPi&JetsonNano/python/lib/waveshare_epd
```

To test, run one of the examples in
`waveshare_epd/RaspberryPi&JetsonNano/python/examples/`.

# About this patch

A caveat of using udev rules is that programs controlling the devices under
udev need to be aware of that and apply custom delays. This is because

> the udev rules take some time to apply.

– Oliv Luca, https://forum.armbian.com/topic/8714-gpio-not-working-for-non-root/

The changes have been tested on NanoPi NEO with the 2.7 e-Paper HAT (B) from
Waveshare.

This patch brings a few tiny improvements to the great package supplied by
Waveshare and Contributors (BIG thanks!). Running without sudo was possible
thanks to the Libgpiod library written by Bartosz Gołaszewski (BIG thanks,
Bartosz!).

I decided to make my changes available as a separate repo. If you are one of
the original authors, please reach out and I will be happy to work with you to
merge the key functionality from here to the original repo.

# Preparing the environment

The following packages need to be installed for Gpiod-based access to work:

- PIL/Pillow,
- libgpiod2,
- python-periphery.

You can run the following script to install them (tested on Ubuntu):

```bash
sudo apt-get update
sudo apt-get install python3-pip
sudo apt-get install python3-pil
sudo apt-get install libgpiod2
sudo pip3 install python-periphery
```
