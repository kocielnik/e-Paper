# /*****************************************************************************
# * | File        :	  epdconfig.py
# * | Author      :   Waveshare team
# * | Function    :   Hardware underlying interface
# * | Info        :
# *----------------
# * | This version:   V1.0
# * | Date        :   2019-06-21
# * | Info        :   
# ******************************************************************************
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documnetation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to  whom the Software is
# furished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS OR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.
#

import os
import logging
import sys
import time

from periphery import GPIO as PeripheryGPIO

class GPIODChip:

    def __init__(self,
                 gpio_chip = "/dev/gpiochip0",
                 pins = {
                     "rst": 0,
                     "dc": 1,
                     "cs": 67,
                     "busy": 201
                 },
                 GPIO = PeripheryGPIO):

        self.save_pins(pins)
        self.init_interfaces(gpio_chip, GPIO)

    def save_pins(self, pins):

        self.rst_pin = pins['rst']
        self.dc_pin = pins['dc']
        self.cs_pin = pins['cs']
        self.busy_pin = pins['busy']

    def init_interfaces(self, gpio_chip, GPIO):

        self.rst = GPIO(gpio_chip, self.rst_pin, "out")
        self.dc = GPIO(gpio_chip, self.dc_pin, "out")
        self.cs = GPIO(gpio_chip, self.cs_pin, "out")
        self.busy = GPIO(gpio_chip, self.busy_pin, "in")

    def output(self, pin, value):
        value = bool(value)
        if pin == self.rst_pin:
            self.rst.write(value)
        if pin == self.dc_pin:
            self.dc.write(value)
        if pin == self.cs_pin:
            self.cs.write(value)

    def input(self, pin):
        if pin == self.busy_pin:
            return self.busy.read()

class RaspberryPi:
    # Pin definition
    RST_PIN         = 17
    DC_PIN          = 25
    CS_PIN          = 8
    BUSY_PIN        = 24

    def __init__(self):
        import spidev
        import RPi.GPIO

        self.GPIO = RPi.GPIO

        # SPI device, bus = 0, device = 0
        self.SPI = spidev.SpiDev(0, 0)

    def digital_write(self, pin, value):
        self.GPIO.output(pin, value)

    def digital_read(self, pin):
        return self.GPIO.input(pin)

    def delay_ms(self, delaytime):
        time.sleep(delaytime / 1000.0)

    def spi_writebyte(self, data):
        self.SPI.writebytes(data)

    def module_init(self):
        self.GPIO.setmode(self.GPIO.BCM)
        self.GPIO.setwarnings(False)
        self.GPIO.setup(self.RST_PIN, self.GPIO.OUT)
        self.GPIO.setup(self.DC_PIN, self.GPIO.OUT)
        self.GPIO.setup(self.CS_PIN, self.GPIO.OUT)
        self.GPIO.setup(self.BUSY_PIN, self.GPIO.IN)
        self.SPI.max_speed_hz = 4000000
        self.SPI.mode = 0b00
        return 0

    def module_exit(self):
        logging.debug("spi end")
        self.SPI.close()

        logging.debug("close 5V, Module enters 0 power consumption ...")
        self.GPIO.output(self.RST_PIN, 0)
        self.GPIO.output(self.DC_PIN, 0)

        self.GPIO.cleanup()

class JetsonNano:
    # Pin definition
    RST_PIN         = 17
    DC_PIN          = 25
    CS_PIN          = 8
    BUSY_PIN        = 24

    def __init__(self):
        import ctypes
        find_dirs = [
            os.path.dirname(os.path.realpath(__file__)),
            '/usr/local/lib',
            '/usr/lib',
        ]
        self.SPI = None
        for find_dir in find_dirs:
            so_filename = os.path.join(find_dir, 'sysfs_software_spi.so')
            if os.path.exists(so_filename):
                self.SPI = ctypes.cdll.LoadLibrary(so_filename)
                break
        if self.SPI is None:
            raise RuntimeError('Cannot find sysfs_software_spi.so')

        import Jetson.GPIO
        self.GPIO = Jetson.GPIO

    def digital_write(self, pin, value):
        self.GPIO.output(pin, value)

    def digital_read(self, pin):
        return self.GPIO.input(self.BUSY_PIN)

    def delay_ms(self, delaytime):
        time.sleep(delaytime / 1000.0)

    def spi_writebyte(self, data):
        self.SPI.SYSFS_software_spi_transfer(data[0])

    def module_init(self):
        self.GPIO.setmode(self.GPIO.BCM)
        self.GPIO.setwarnings(False)
        self.GPIO.setup(self.RST_PIN, self.GPIO.OUT)
        self.GPIO.setup(self.DC_PIN, self.GPIO.OUT)
        self.GPIO.setup(self.CS_PIN, self.GPIO.OUT)
        self.GPIO.setup(self.BUSY_PIN, self.GPIO.IN)
        self.SPI.SYSFS_software_spi_begin()
        return 0

    def module_exit(self):
        logging.debug("spi end")

class NanoPi:

    @staticmethod
    def is_detected():

        """
        Detects if the current platform is a NanoPi.

        Example with a test:

        >>> isinstance(NanoPi.is_detected(), bool)
        True
        """

        info_path = "/sys/firmware/devicetree/base/model"

        if not os.path.exists(info_path):
            return False

        with open(info_path, 'r') as infile:
            model = infile.read()

            return ("NanoPi" in model)

    class Gpiod:
        # Pin definition
        RST_PIN     = 0      # BOARD: 11
        DC_PIN      = 1      # BOARD: 22
        CS_PIN      = 67     # BOARD: 24
        BUSY_PIN    = 201    # BOARD: 18

        SPI_DEVICE = "/dev/spidev0.0"

        def __init__(self):

            # Prefer GPIOD if present. Fall back to SysFs if necessary.
            try:
                self.GPIO = GPIODChip()
            except AttributeError as e:
                print("Unable to initialize GPIOD subsystem.")
                raise e
                import NPi.GPIO
                self.GPIO = NPi.GPIO

            try:
                from periphery import SPI
                self.SPI = SPI(self.SPI_DEVICE, 0b00, 4000000)
            except:
                print("Unable to initialize Periphery SPI subsystem.")
                import spidev
                # SPI device, bus = 0, device = 0
                self.SPI = spidev.SpiDev(0, 0)

        def digital_write(self, pin, value):
            self.GPIO.output(pin, value)

        def digital_read(self, pin):
            return self.GPIO.input(pin)

        def delay_ms(self, delaytime):
            time.sleep(delaytime / 1000.0)

        def spi_writebyte(self, data):
            self.SPI.transfer(data)

        def setup_sysfs(self):
            self.GPIO.setmode(self.GPIO.RAW)
            self.GPIO.setwarnings(False)
            self.GPIO.setup(self.RST_PIN, self.GPIO.OUT)
            self.GPIO.setup(self.DC_PIN, self.GPIO.OUT)
            self.GPIO.setup(self.CS_PIN, self.GPIO.OUT)
            self.GPIO.setup(self.BUSY_PIN, self.GPIO.IN)

        def module_init(self):
            try:
                self.setup_sysfs()
            except AttributeError as e:
                logging.debug("GPIOD detected - aborting Sysfs configuration.")
            self.SPI.max_speed_hz = 4000000
            self.SPI.mode = 0b00
            return 0

        def module_exit(self):
            logging.debug("spi end")
            self.SPI.close()

            logging.debug("close 5V, Module enters 0 power consumption ...")
            self.GPIO.output(self.RST_PIN, 0)
            self.GPIO.output(self.DC_PIN, 0)

    class Sysfs:
        # Pin definition
        RST_PIN     = 0      # BOARD: 11
        DC_PIN      = 1      # BOARD: 22
        CS_PIN      = 67     # BOARD: 24
        BUSY_PIN    = 201    # BOARD: 18

        def __init__(self):
            import spidev
            import NPi.GPIO

            self.GPIO = NPi.GPIO

            # SPI device, bus = 0, device = 0
            self.SPI = spidev.SpiDev(0, 0)

        def digital_write(self, pin, value):
            self.GPIO.output(pin, value)

        def digital_read(self, pin):
            return self.GPIO.input(pin)

        def delay_ms(self, delaytime):
            time.sleep(delaytime / 1000.0)

        def spi_writebyte(self, data):
            self.SPI.writebytes(data)

        def module_init(self):
            self.GPIO.setmode(self.GPIO.RAW)
            self.GPIO.setwarnings(False)
            self.GPIO.setup(self.RST_PIN, self.GPIO.OUT)
            self.GPIO.setup(self.DC_PIN, self.GPIO.OUT)
            self.GPIO.setup(self.CS_PIN, self.GPIO.OUT)
            self.GPIO.setup(self.BUSY_PIN, self.GPIO.IN)
            self.SPI.max_speed_hz = 4000000
            self.SPI.mode = 0b00
            return 0

        def module_exit(self):
            logging.debug("spi end")
            self.SPI.close()

            logging.debug("close 5V, Module enters 0 power consumption ...")
            self.GPIO.output(self.RST_PIN, 0)
            self.GPIO.output(self.DC_PIN, 0)

            self.GPIO.cleanup()

### END OF FILE ###
