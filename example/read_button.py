#!/usr/bin/env python3

from periphery import GPIO
from os import system

button = GPIO("/dev/gpiochip0", 203, "in")  # pin 36

button.edge = "rising"

try:
    if button.poll():
        print("Shutdown button pressed, shutting down.")
        system('/usr/sbin/shutdown now')
finally:
    button.close()
