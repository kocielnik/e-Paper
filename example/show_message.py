#!/usr/bin/env python3

import argparse
from sys import stdin, argv
import sys
from PIL import ImageFont, ImageDraw, Image
from contextlib import contextmanager
from os.path import dirname, join

def add_relative_import_path(import_dir):
    new_path = join(dirname(__file__), import_dir)
    sys.path.append(new_path)

@contextmanager
def get_managed(driver):

    epd = driver.EPD()
    epd.init()

    try:
        yield epd
        epd.sleep()
    except KeyboardInterrupt:
        driver.epdconfig.module_exit()

def get_horizontal_image(message, height, width, font_size):

    font18 = ImageFont.truetype(join(dirname(__file__), "Font.ttc"), font_size)

    clear_frame_code = 255
    Himage = Image.new("1", (height, width), clear_frame_code)
    draw = ImageDraw.Draw(Himage)
    draw.text((10, 5), message, font=font18, fill=0)

    return Himage

def draw_text_horizontal(message, epd, font_size):

    image = get_horizontal_image(message, epd.height, epd.width, font_size)
    epd.display(epd.getbuffer(image))

def print_message(font_size):
    add_relative_import_path('..')
    import epd2in7 as epd_driver

    message = stdin.read()

    with get_managed(epd_driver) as epd:
        draw_text_horizontal(message, epd, font_size)

def get_command_line_args():

    parser = argparse.ArgumentParser(
        description='Show a message from standard input on e-paper.',
        usage='{} <<< MESSAGE'.format(argv[0])
    )

    parser.add_argument(
        '--font-size', type=int, action='store', default=18,
        help='Font size, default=18'
    )

    args = parser.parse_args()

    return args

def main():
    print_message(get_command_line_args().font_size)

if __name__ == '__main__':
    main()
