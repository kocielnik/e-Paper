#!/usr/bin/env pytest

from unittest.mock import MagicMock
from boards import GPIODChip

def test_gpiod():
    mock_gpio = MagicMock()
    chip = GPIODChip(GPIO = mock_gpio)

    mock_gpio.assert_called_with('/dev/gpiochip0', 201, 'in')
