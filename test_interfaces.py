#!/usr/bin/env pytest

from periphery import GPIO, SPI
from contextlib import contextmanager
from os import path
from pytest import mark
import signal

# Run this to check if read-write to GPIO/SPI/EPD on NanoPi is possible.

@mark.skipif(not path.exists("/dev/gpiochip0"),
             reason="Requires a GPIO chip device.")
def test_nanopi_gpio():
    # Open GPIO /dev/gpiochip0 line 12 with output direction
    gpio_out = GPIO("/dev/gpiochip1", 11, "out")
    # Open GPIO /dev/gpiochip0 line 10 with input direction
    gpio_in = GPIO("/dev/gpiochip0", 20, "in")

    value = gpio_in.read()
    gpio_out.write(not value)

    gpio_in.close()
    gpio_out.close()

    print("GPIO R/W verified.")

@mark.skipif(not path.exists("/dev/spidev0.0"),
             reason="Requires an SPI device.")
def test_nanopi_spi():
    # Open spidev1.0 with mode 0 and max speed 1MHz
    spi = SPI("/dev/spidev0.0", 0, 1000000)

    data_out = [0xaa, 0xbb, 0xcc, 0xdd]
    data_in = spi.transfer(data_out)

    log = "shifted out [0x{:02x}, 0x{:02x}, 0x{:02x}, 0x{:02x}]".format(
        *data_out)
    log += "shifted in  [0x{:02x}, 0x{:02x}, 0x{:02x}, 0x{:02x}]".format(
        *data_in)

    spi.close()

    print("SPI R/W verified.")

@contextmanager
def get_managed(driver):

    epd = driver.EPD()
    epd.init()

    try:
        yield epd
        epd.sleep()
    except KeyboardInterrupt:
        driver.epdconfig.module_exit()

class timeout:

    """
    Credit: David Narayan, Thomas Ahle
    https://stackoverflow.com/a/22348885
    """

    def __init__(self, seconds=1, error_message='Timeout'):
        self.seconds = seconds
        self.error_message = error_message
    def handle_timeout(self, signum, frame):
        raise TimeoutError(self.error_message)
    def __enter__(self):
        signal.signal(signal.SIGALRM, self.handle_timeout)
        signal.alarm(self.seconds)
    def __exit__(self, type, value, traceback):
        signal.alarm(0)

@mark.skipif(not (
        path.exists("/dev/spidev0.0") and path.exists("/dev/gpiochip0")
    ),
    reason="Requires an SPI device.")
def test_epd_reachable():
    import epd2in7 as epd_driver
    with timeout(seconds=3):
        with get_managed(epd_driver):
            pass
    print("EPD R/W verified.")
